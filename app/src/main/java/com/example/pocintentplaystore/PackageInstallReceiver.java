package com.example.pocintentplaystore;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

public class PackageInstallReceiver extends BroadcastReceiver {

    private static final String TAG = "NewAppReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if (Intent.ACTION_PACKAGE_ADDED.equals(action)) {
            // Obter o URI do pacote instalado
            String dataString = intent.getDataString();
            Log.d("YourReceiver", "Pacote adicionado: " + dataString);

            // Se precisar, você pode extrair o nome do pacote da URI
            String packageName = intent.getData().getSchemeSpecificPart();
            if (!isAppAllowed(packageName)){
                uninstallApp(context, packageName);
            }
            Log.d("YourReceiver", "Nome do pacote: " + packageName);
        }
    }

    private boolean isAppAllowed(String packageName) {
        // Lógica para verificar se o aplicativo está na lista permitida
        if (packageName.equals("com.instagram.android")){
            Log.d("YourReceiver", "App instagram permitido" + packageName);
            return true;
        }
        // Retorne true se estiver permitido, false caso contrário
        Log.d("YourReceiver", "App " + packageName + " não permitido");
        return false;
    }

    private void uninstallApp(Context context, String packageName) {
        // Inicie o processo de desinstalação
//        Intent uninstallIntent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
//        uninstallIntent.setData(Uri.parse("package:" + packageName));
//        uninstallIntent.putExtra(Intent.EXTRA_RETURN_RESULT, true);
//        context.startActivity(uninstallIntent);
        Intent intent = new Intent(Intent.ACTION_DELETE);
        intent.setData(Uri.parse("package:" + packageName));
        context.startActivity(intent);


    }
}
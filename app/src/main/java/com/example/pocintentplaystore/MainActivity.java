package com.example.pocintentplaystore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    private PackageInstallReceiver packageInstallReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        try {
//            // Comando que você deseja executar
//            String[] arrayCommand = {"logcat", "|", "grep", "\"Finsky\""};
//
//            // Executa o comando
//            Process process = Runtime.getRuntime().exec(arrayCommand);
//
//            // Lê a saída do comando
//            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
//            BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
//
//            StringBuilder log = new StringBuilder();
//
//            String line = "";
//            while ((line = reader.readLine()) != null) {
//
//                log.append(line);
//                Log.i("saidaLog", "" + log + "/n");
//            }
//
//            String errorLine;
//            while ((errorLine = errorReader.readLine()) != null) {
//                System.out.println("Erro: " + errorLine);
//                Log.i("saidaLog", "" + errorLine);
//            }
//
//            // Aguarda o término do processo
//            int exitCode = process.waitFor();
//            Log.i("saidaLog", "" + exitCode);
//            System.out.println("Saída do comando: " + exitCode);
//
//        } catch (IOException | InterruptedException e) {
//            e.printStackTrace();
//            Log.i("saidaLog", "error no catch" + e);
//        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    //String[] arrayCommand = {"logcat"};
                    String[] arrayCommand = {"shell"};
                    //String[] arrayCommand = {"logcat", "| grep \"Finsky\"" };                    // Executa o comando
                    Process process = Runtime.getRuntime().exec(arrayCommand);

                    // Lê a saída do comando
                    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));

                    StringBuilder log = new StringBuilder();

                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        log.append(line);
                        Log.i("saidaLog", "" + log + "/n");
                    }

                    String errorLine;
                    while ((errorLine = errorReader.readLine()) != null) {
                        System.out.println("Erro: " + errorLine);
                        Log.i("saidaLog", "" + errorLine);
                    }

                    // Aguarda o término do processo
                    int exitCode = process.waitFor();
                    Log.i("saidaLog", "" + exitCode);
                    System.out.println("Saída do comando: " + exitCode);

                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                    Log.i("saidaLog", "error no catch" + e);
                }
            }
        }).start();

//        try {
//            Process process = Runtime.getRuntime().exec("ls");
//            BufferedReader bufferedReader = new BufferedReader(
//                    new InputStreamReader(process.getInputStream()));
//
//            StringBuilder log=new StringBuilder();
//            String line = "";
//            while ((line = bufferedReader.readLine()) != null) {
//                log.append(line);
//
//            }
//
//
//            Log.i("saidaLog", "" + log);
//        }
//        catch (IOException e) {}

        packageInstallReceiver = new PackageInstallReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_PACKAGE_ADDED);
        filter.addDataScheme("package");
        registerReceiver(packageInstallReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(packageInstallReceiver);
    }
}